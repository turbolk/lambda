var https = require('https');
var AWS = require('aws-sdk');

var codedeploy = new AWS.CodeDeploy();

exports.handler = function(event, context, callback) {

    var query = JSON.parse(event.body);

    if (query.head_commit.committer.name !== 'GitHub') return;
    
    const paragraph = query.head_commit.message;
    const regex = /#\d+/g;
    const found = paragraph.match(regex);
    
    if (found == 'null') return; else var pull_request_number = found[0];
	
	//remove hash
	pull_request_number = pull_request_number.replace("#", ""); 

    // An object of options to indicate where to post to
    var post_options = {
        host: 'api.github.com',
        port: '443',
        path: '/repos/miyurusankalpa/turbodev/issues/' + pull_request_number + '/comments',
        method: 'POST',
        headers: {
            'Authorization': 'token ' + process.env.github,
            'Content-Type': 'application/json',
            'User-Agent': 'turbolk lambda'
        }
    };

    var params = {
        applicationName: 'TurboLK',
        /* required */
        deploymentGroupName: 'turbo-dev',
        description: 'Triggered from Github Pull Request #' + pull_request_number + ' ',
        revision: {
            gitHubLocation: {
                commitId: query.head_commit.id,
                repository: 'miyurusankalpa/turbodev'
            },
            revisionType: "GitHub",
        }
    };

    codedeploy.createDeployment(params, function(err, data) {
        if (err) console.log(err, err.stack); // an error occurred
        else {
            console.log(data); // successful response

            // post the data
            post_req.write('{"body": "Deployment Started. ID [' + data.deploymentId + '](https://ap-southeast-1.console.aws.amazon.com/codesuite/codedeploy/deployments/' + data.deploymentId + '?region=ap-southeast-1)"}');
            post_req.end();
        }
    });

    // Set up the request
    var post_req = https.request(post_options, function(res) {
        console.log("Got response: " + res.statusCode);

        res.setEncoding('utf8');
        res.on('data', function(chunk) {
            console.log('Response: ' + chunk);
            context.succeed();
        });
        res.on('error', function(e) {
            console.log("Got error: " + e.message);
            context.done(null, 'FAILURE');
        });

    });



}