import boto3

def lambda_handler(event, context):

  ec2 = boto3.client('ec2')
  asg = boto3.client('autoscaling')
  r53 = boto3.client('route53')

  response = asg.describe_auto_scaling_groups(
      AutoScalingGroupNames=[
          'turbolk-blue',
      ],
      MaxRecords=10
  )

  insids = []

  for asg in response['AutoScalingGroups']:
    for ins in asg['Instances']:
      insids.append(ins['InstanceId'])

  print(insids)


  response = ec2.describe_instances(
      InstanceIds=insids
  )

  ipv4 = []
  ipv6 = []

  for res in response['Reservations']:
    for ins in res['Instances']:
      ipv4.append({'Value': ins['PublicIpAddress'] })
      for net in ins['NetworkInterfaces']:
        for v6 in net['Ipv6Addresses']:
          ipv6.append({'Value': v6['Ipv6Address'] })

  print(ipv4)
  print(ipv6)

  domain = "ppe.turbo.lk"

  response = r53.change_resource_record_sets(
      HostedZoneId='ZFZ3RNA6H5VD6',
      ChangeBatch={
          'Comment': 'auto dns update for asg',
          'Changes': [
              {
                  'Action': 'UPSERT',
                  'ResourceRecordSet': {
                      'Name': domain,
                      'Type': 'A',
                      'TTL': 300,
                      'ResourceRecords': ipv4
                  }
              },
        {
                  'Action': 'UPSERT',
                  'ResourceRecordSet': {
                      'Name': domain,
                      'Type': 'AAAA',
                      'TTL': 300,
                      'ResourceRecords': ipv6
                  }
              },
          ]
      }
  )

  print(response)  